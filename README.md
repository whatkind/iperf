1. 下载后解压**iperf_tool.zip**
2. 点击`jperf.bat`运行

iperf3下载地址
https://iperf.fr/iperf-download.php

开启服务端
```sh
iperf3 -s
#iperf3 -s --logfile iperf3.log
```
开启客户端（发送端）
```sh
iperf3 -c 127.0.0.1 -t 5
```
